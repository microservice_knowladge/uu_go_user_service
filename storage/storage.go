package storage

import (
	"context"

	"microservice_knowladge/uu_go_user_service/genproto/user_service"
	"microservice_knowladge/uu_go_user_service/models"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
	Hobby() HobbyRepoI
}

type UserRepoI interface {
	Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error)
	GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateUser) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.UserPrimaryKey) error
}

type HobbyRepoI interface {
	Create(ctx context.Context, req *user_service.CreateHobbies) (resp *user_service.HobbiesPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.HobbiesPrimaryKey) (resp *user_service.Hobbies, err error)
	GetAll(ctx context.Context, req *user_service.GetListHobbiesRequest) (resp *user_service.GetListHobbiesResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateHobbies) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.HobbiesPrimaryKey) error
}
