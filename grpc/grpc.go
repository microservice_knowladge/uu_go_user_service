package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"microservice_knowladge/uu_go_user_service/config"
	"microservice_knowladge/uu_go_user_service/genproto/user_service"
	"microservice_knowladge/uu_go_user_service/grpc/client"
	"microservice_knowladge/uu_go_user_service/grpc/service"
	"microservice_knowladge/uu_go_user_service/pkg/logger"
	"microservice_knowladge/uu_go_user_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))
	user_service.RegisterHobbyServiceServer(grpcServer, service.NewHobbiesService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
