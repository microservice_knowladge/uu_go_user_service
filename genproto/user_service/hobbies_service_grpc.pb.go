// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.12.4
// source: hobbies_service.proto

package user_service

import (
	context "context"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// HobbyServiceClient is the client API for HobbyService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type HobbyServiceClient interface {
	Create(ctx context.Context, in *CreateHobbies, opts ...grpc.CallOption) (*Hobbies, error)
	GetByID(ctx context.Context, in *HobbiesPrimaryKey, opts ...grpc.CallOption) (*Hobbies, error)
	GetList(ctx context.Context, in *GetListHobbiesRequest, opts ...grpc.CallOption) (*GetListHobbiesResponse, error)
	Update(ctx context.Context, in *UpdateHobbies, opts ...grpc.CallOption) (*Hobbies, error)
	UpdatePatch(ctx context.Context, in *UpdatePatchHobbies, opts ...grpc.CallOption) (*Hobbies, error)
	Delete(ctx context.Context, in *HobbiesPrimaryKey, opts ...grpc.CallOption) (*empty.Empty, error)
}

type hobbyServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewHobbyServiceClient(cc grpc.ClientConnInterface) HobbyServiceClient {
	return &hobbyServiceClient{cc}
}

func (c *hobbyServiceClient) Create(ctx context.Context, in *CreateHobbies, opts ...grpc.CallOption) (*Hobbies, error) {
	out := new(Hobbies)
	err := c.cc.Invoke(ctx, "/hobbies.HobbyService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *hobbyServiceClient) GetByID(ctx context.Context, in *HobbiesPrimaryKey, opts ...grpc.CallOption) (*Hobbies, error) {
	out := new(Hobbies)
	err := c.cc.Invoke(ctx, "/hobbies.HobbyService/GetByID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *hobbyServiceClient) GetList(ctx context.Context, in *GetListHobbiesRequest, opts ...grpc.CallOption) (*GetListHobbiesResponse, error) {
	out := new(GetListHobbiesResponse)
	err := c.cc.Invoke(ctx, "/hobbies.HobbyService/GetList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *hobbyServiceClient) Update(ctx context.Context, in *UpdateHobbies, opts ...grpc.CallOption) (*Hobbies, error) {
	out := new(Hobbies)
	err := c.cc.Invoke(ctx, "/hobbies.HobbyService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *hobbyServiceClient) UpdatePatch(ctx context.Context, in *UpdatePatchHobbies, opts ...grpc.CallOption) (*Hobbies, error) {
	out := new(Hobbies)
	err := c.cc.Invoke(ctx, "/hobbies.HobbyService/UpdatePatch", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *hobbyServiceClient) Delete(ctx context.Context, in *HobbiesPrimaryKey, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/hobbies.HobbyService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// HobbyServiceServer is the server API for HobbyService service.
// All implementations must embed UnimplementedHobbyServiceServer
// for forward compatibility
type HobbyServiceServer interface {
	Create(context.Context, *CreateHobbies) (*Hobbies, error)
	GetByID(context.Context, *HobbiesPrimaryKey) (*Hobbies, error)
	GetList(context.Context, *GetListHobbiesRequest) (*GetListHobbiesResponse, error)
	Update(context.Context, *UpdateHobbies) (*Hobbies, error)
	UpdatePatch(context.Context, *UpdatePatchHobbies) (*Hobbies, error)
	Delete(context.Context, *HobbiesPrimaryKey) (*empty.Empty, error)
	mustEmbedUnimplementedHobbyServiceServer()
}

// UnimplementedHobbyServiceServer must be embedded to have forward compatible implementations.
type UnimplementedHobbyServiceServer struct {
}

func (UnimplementedHobbyServiceServer) Create(context.Context, *CreateHobbies) (*Hobbies, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedHobbyServiceServer) GetByID(context.Context, *HobbiesPrimaryKey) (*Hobbies, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetByID not implemented")
}
func (UnimplementedHobbyServiceServer) GetList(context.Context, *GetListHobbiesRequest) (*GetListHobbiesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedHobbyServiceServer) Update(context.Context, *UpdateHobbies) (*Hobbies, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedHobbyServiceServer) UpdatePatch(context.Context, *UpdatePatchHobbies) (*Hobbies, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdatePatch not implemented")
}
func (UnimplementedHobbyServiceServer) Delete(context.Context, *HobbiesPrimaryKey) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedHobbyServiceServer) mustEmbedUnimplementedHobbyServiceServer() {}

// UnsafeHobbyServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to HobbyServiceServer will
// result in compilation errors.
type UnsafeHobbyServiceServer interface {
	mustEmbedUnimplementedHobbyServiceServer()
}

func RegisterHobbyServiceServer(s grpc.ServiceRegistrar, srv HobbyServiceServer) {
	s.RegisterService(&HobbyService_ServiceDesc, srv)
}

func _HobbyService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateHobbies)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HobbyServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hobbies.HobbyService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HobbyServiceServer).Create(ctx, req.(*CreateHobbies))
	}
	return interceptor(ctx, in, info, handler)
}

func _HobbyService_GetByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(HobbiesPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HobbyServiceServer).GetByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hobbies.HobbyService/GetByID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HobbyServiceServer).GetByID(ctx, req.(*HobbiesPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

func _HobbyService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetListHobbiesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HobbyServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hobbies.HobbyService/GetList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HobbyServiceServer).GetList(ctx, req.(*GetListHobbiesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HobbyService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateHobbies)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HobbyServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hobbies.HobbyService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HobbyServiceServer).Update(ctx, req.(*UpdateHobbies))
	}
	return interceptor(ctx, in, info, handler)
}

func _HobbyService_UpdatePatch_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdatePatchHobbies)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HobbyServiceServer).UpdatePatch(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hobbies.HobbyService/UpdatePatch",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HobbyServiceServer).UpdatePatch(ctx, req.(*UpdatePatchHobbies))
	}
	return interceptor(ctx, in, info, handler)
}

func _HobbyService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(HobbiesPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HobbyServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hobbies.HobbyService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HobbyServiceServer).Delete(ctx, req.(*HobbiesPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

// HobbyService_ServiceDesc is the grpc.ServiceDesc for HobbyService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var HobbyService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "hobbies.HobbyService",
	HandlerType: (*HobbyServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _HobbyService_Create_Handler,
		},
		{
			MethodName: "GetByID",
			Handler:    _HobbyService_GetByID_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _HobbyService_GetList_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _HobbyService_Update_Handler,
		},
		{
			MethodName: "UpdatePatch",
			Handler:    _HobbyService_UpdatePatch_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _HobbyService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "hobbies_service.proto",
}
